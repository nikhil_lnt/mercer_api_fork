{
  "entities": [
    {
      "data": [
        {
		  "EXP_COMP2": "1000",
          "EMP_125": "10",
          "YOUR_EEID": "101",
          "POS_CLASS": "52",
          "POS_CODE": "HRM.03.002.M20",
          "EMP_129": "",
          "EMP_128": "10"
        },
        {
		  "EXP_COMP2": "1010",
          "EMP_125": "32000",
          "YOUR_EEID": "102",
          "POS_CLASS": "52",
          "POS_CODE": "HRM.03.002.M20",
          "EMP_129": "101"
        },
        {
		  "EXP_COMP2": "990",
          "EMP_125": "32000",
          "YOUR_EEID": "103",
          "POS_CLASS": "55",
          "POS_CODE": "ABC.03.002.M20",
          "EMP_129": "99"
        },
        {
		  "EXP_COMP2": "980",
          "EMP_125": "32000",
          "YOUR_EEID": "104",
          "POS_CLASS": "55",
          "POS_CODE": "ABC.03.002.M20",
          "EMP_129": "98"
        },
        {
		  "EXP_COMP2": "1990",
          "EMP_125": "32000",
          "YOUR_EEID": "105",
          "POS_CLASS": "52",
          "POS_CODE": "ABC.03.002.S10",
          "EMP_129": "199"
        },
        {
		  "EXP_COMP2": "2010",
          "EMP_125": "32000",
          "YOUR_EEID": "106",
          "POS_CLASS": "52",
          "POS_CODE": "HRM.03.002.S10",
          "EMP_129": "201"
        },
        {
		  "EXP_COMP2": "520",
          "EMP_125": "32000",
          "YOUR_EEID": "107",
          "POS_CLASS": "52",
          "POS_CODE": "XYZ.03.002.P60",
          "EMP_129": "52"
        },
        {
		  "EXP_COMP2": "480",
          "EMP_125": "32000",
          "YOUR_EEID": "108",
          "POS_CLASS": "52",
          "POS_CODE": "HRM.03.002.P60",
          "EMP_129": "48"
        },
        {
		  "EXP_COMP2": "400",
          "EMP_125": "32000",
          "YOUR_EEID": "109",
          "POS_CLASS": "52",
          "POS_CODE": "XYZ.03.002.E02",
          "EMP_129": "40"
        }
      ],
      "sectionStructure": {
        "columns": [
          {
            "code": "EMP_125",
            "displayLabel": "Monthly Base Salary (Equivalent to 100% work time)",
            "dataType": "int",
            "questionType": "integer",
            "validations": [
              {
                "errorType": "ERROR",
                "validationType": "expression",
                "expression": "INVERSIONCHECK(XAVG(\"EMP_125\", \"EMP_125 >= 10\" , \"POS_CLASS\"));",
                "errorGroup": "Personal",
                "message": "Length of Department"
              }
            ]
          }
        ]
      },
      "otherSectionsData": {
        
      },
      "contextData": {
        "campaignId": "599820d4747cf240d90c0b8c",
        "companyId": "599820d4747cf240d90c0b8d",
        "sectionId": "incumbent_data",
        "grpCode": "",
        "cpyCode": "",
        "ctryCode": "AU",
        "orgSize": 20,
        "uniqueIdColumnCode": "YOUR_EEID",
        "industry": {
          "superSector": "HT",
          "sector": "196",
          "subSector": "2070"
        }
      }
    }
  ]
}
