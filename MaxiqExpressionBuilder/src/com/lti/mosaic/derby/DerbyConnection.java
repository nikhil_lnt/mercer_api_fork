package com.lti.mosaic.derby;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.lti.mosaic.function.def.LookUpFunctionBso;
import com.lti.mosaic.parser.constants.LoggerConstants;


/**
 * @author rushikesh
 *
 */
public class DerbyConnection {


  private static final String FORMAT_CONSTANT = "{} {}";


private static final Logger logger = LoggerFactory.getLogger(DerbyConnection.class);


  private static DerbyConnection jdbc;
  static Connection derbyConnection = null;
  static Connection mysqlConn = null;
  private static final String GET = " : >> get()";

  public static DerbyConnection getJdbc() {
  	return jdbc;
  }
  
  public static void setJdbc(DerbyConnection jdbc) {
  	DerbyConnection.jdbc = jdbc;
  }
  
  public static Connection getMysqlConn() {
  	return mysqlConn;
  }
  
  public static void setMysqlConn(Connection mysqlConn) {
  	DerbyConnection.mysqlConn = mysqlConn;
  }
  
  private static Map<String, List<List<Object>>> cachedResults =
      new HashMap<>();

  // JDBCSingleton prevents the instantiation from any other class.
  private DerbyConnection() {}

  // Providing global point of access.
  public static DerbyConnection getInstance() {
    logger.info("{} : >> getInstance()",LoggerConstants.LOG_MAXIQAPI);
    if (jdbc == null) {
      jdbc = new DerbyConnection();
    }

    logger.info("{} : << getInstance()",LoggerConstants.LOG_MAXIQAPI);
    return jdbc;
  }

  // to view the data from the database
  // For PreparedStatement use
  // to view the data from the database
  // For PreparedStatement use
  public ResultSet get(Connection conn, String query, String[] args) throws SQLException {
    logger.info(FORMAT_CONSTANT,LoggerConstants.LOG_MAXIQAPI, GET);
    PreparedStatement pStmt = null;
    ResultSet rs = null;
    try {
      pStmt = conn.prepareStatement(query);
      if (args != null) {
        for (int i = 0; i < args.length; i++) {
          pStmt.setObject(i + 1, args[i]);
        }
      }
      rs = pStmt.executeQuery();
    } catch (Exception e) {
      logger.error("{}  : >> get() {}",LoggerConstants.LOG_MAXIQAPI, e.getMessage());
    }
    logger.info(FORMAT_CONSTANT,LoggerConstants.LOG_MAXIQAPI, GET);
    return rs;
  }

  // For Statement use
  public List<List<Object>> get(Connection conn, String query) throws SQLException {
    logger.info(FORMAT_CONSTANT,LoggerConstants.LOG_MAXIQAPI, GET);
    List<List<Object>> output = null;
    Statement stmt = null;
    ResultSet rs = null;
    if (cachedResults.containsKey(query)){
      output = cachedResults.get(query);
    } else {
      try {
        stmt = conn.createStatement();
        rs = stmt.executeQuery(query);
          output = new LookUpFunctionBso().parseResultSet(rs);
      } catch (SQLException e) {
        logger.error(LoggerConstants.LOG_MAXIQAPI + " : >> get() {}", e);
      } finally {
        cachedResults.put(query, output);
        if (null != rs) {
          try {
            rs.close();
          } catch (SQLException e) {
            logger.error(e.getMessage());
          }
        }
        if (null != stmt) {
          try {
            stmt.close();
          } catch (SQLException e) {
            logger.error(e.getMessage());
          }
        }
      }
    }
    logger.info(FORMAT_CONSTANT,LoggerConstants.LOG_MAXIQAPI, GET);
    
    return output;
  }

}
