package in.lnt.utility.general;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.Properties;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import in.lnt.utility.constants.CacheConstats;
import in.lnt.utility.constants.MercerErrorConstants;


public class Cache  implements MercerErrorConstants,Serializable {
	
	private static final Logger logger = LoggerFactory.getLogger(Cache.class);
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static Properties config = new Properties();
	private static Properties mlConfig = new Properties();
	private static volatile Cache cache;

	static {
		 String base = System.getenv().get(CacheConstats.MAXIQ_HOME);
		 String mlBase = System.getenv().get(CacheConstats.API_HOME);
		FileInputStream fis=null;
		FileInputStream fisML=null;
		
		logger.info("BasePath    =====  {}" , base);
		if (StringUtils.isBlank(base)) {
			logger.info("{}","Base Path $MAXIQ_HOMEnot configured");
		}

		String basePath = base + "/conf/config.properties";
		logger.info(basePath);
		try {
			
			fis = FileUtils.openInputStream(new File(basePath));
			fisML = FileUtils.openInputStream(new File(mlBase+"/conf/config.properties"));
				config.load(fis);
				mlConfig.load(fisML);
		} 
		catch (IOException e) {
			logger.error("IOException occured {}",e.getMessage());
		}
		finally
		{
		
			try {
				fis.close();
				fisML.close();
			} catch (IOException e) {
				logger.error("IOException occured {}",e.getMessage());
			}
		}
	}

	public static Cache getInstance() {
		if (null == cache) {
			synchronized (Cache.class) {
				if (null == cache) {
					cache = new Cache();
				}
			}
		}

		return cache;
	}

	/***
	 * Gets the cache value for the key
	 *
	 * @param key
	 * @return
	 */
	public static String getProperty(String key) {
		return config.getProperty(key);
	}
	
	public static String getPropertyFromError(String key) {
		return Cache.getInstance().getError(key);
		
	}
	
	public static String getMLProperty(String key) {
		return mlConfig.getProperty(key);
		
	}
	
}
