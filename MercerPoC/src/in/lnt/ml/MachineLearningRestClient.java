package in.lnt.ml;

import java.io.IOException;
import java.nio.charset.Charset;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.lnt.utility.constants.CacheConstats;
import in.lnt.utility.general.Cache;


/**
 * 
 * @author Sarang Gandhi
 *  Class to connect Machine Learning Executor Flask Server
 */

public class MachineLearningRestClient {
	private MachineLearningRestClient(){
		
	}

	private static final Logger logger = LoggerFactory.getLogger(MachineLearningRestClient.class);
	
	private static String baseURL = Cache.getMLProperty(CacheConstats.URL_AUTOCORRECTION);
	
	
 public static boolean isFlaskServerOn() {
		 
		 boolean flag = false;
		// Create HTTP header
		HttpHeaders headers = new HttpHeaders();
		
		// Create restTemplate
		RestTemplate restTemplate = new RestTemplate();
		
		 HttpEntity<String> requestEntity = new HttpEntity<>("", headers);
	  try {
		  headers.setContentType(MediaType.APPLICATION_JSON);
		 
		  ResponseEntity<String> response = restTemplate.exchange(baseURL, HttpMethod.POST, requestEntity, String.class);
		  
		  if("AUTOCORRECTION OK".equalsIgnoreCase(response.getBody())) {
			  flag = true;
		  }
	  }catch (Exception e) {
			
		  logger.error("Not able to connect flask server{}",e.getMessage());
		  flag = false;
		}
		return  flag;
		 
	 }

  /**
   *  This method will hit flask server for the python-ML based micro-service.
   * @param entityNode
   * @return JsonNode 
   */
	public static JsonNode callMLRestAPI(JsonNode entityNode) {

		MultiValueMap<String, Object> bodyMap = new LinkedMultiValueMap<>();
		JsonNode jsonNode = null;
		ObjectMapper mapper = new ObjectMapper();
		// Create HTTP header
		HttpHeaders headers = new HttpHeaders();
		// Create restTemplate
		RestTemplate restTemplate = new RestTemplate();
		// String for the response body
		String responseBody = null;
		restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8"))); // PE-8584
		// Add metadata and data inot the body map.
			bodyMap.add("", entityNode.toString());
		if (null == baseURL) {
			baseURL = "http://localhost:5000/upload";
		}
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> requestEntity = new HttpEntity<>(entityNode.toString(), headers);
		try {
			ResponseEntity<String> response = restTemplate.exchange(baseURL, HttpMethod.POST, requestEntity, String.class);
			responseBody = response.getBody();
			
			if (null != responseBody && !"ML FAILED".equalsIgnoreCase(responseBody)) {
				jsonNode = mapper.readTree(response.getBody());
			}
		} catch (IOException e) {
			  logger.error("IOException occured reason is:{}",e.getMessage());
		}catch (Exception e) {
			  logger.error("Generic exception occured reason is:{} ",e.getMessage());
		}
		return jsonNode;
	}
}
