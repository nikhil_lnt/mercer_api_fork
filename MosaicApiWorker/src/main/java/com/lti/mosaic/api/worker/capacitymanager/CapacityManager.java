package com.lti.mosaic.api.worker.capacitymanager;

import in.lti.mosaic.api.base.configs.Cache;
import in.lti.mosaic.api.base.configs.CacheConstants;

/**
 * 
 * @author rushi
 */
public class CapacityManager {
	
	 /**
	   * @author Akhil 
	  */
		private CapacityManager() {
			}

  /**
   * max parallel process per worker
   */
  public static final Long MAXCAPACITY =
      Long.valueOf(Cache.getProperty(CacheConstants.MAX_CAPACITY_PER_WORKER));

  static Long currentProcesses;

  static {
    init();
  }

  public static void init() {
    currentProcesses = 0l;
  }

  /**
   * 
   * @return Boolean - true : if no of current Processes are less than maxCapacity else false
   */
  public static Boolean canITakeNewTask() {
    if (currentProcesses < MAXCAPACITY) {
      currentProcesses++;
      return true;
    } else {
      return false;
    }
  }

  /**
   * it will reduce the no of tasks currently in progress
   */
  public static void workCompleted() {
    currentProcesses--;
  }
}